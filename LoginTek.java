package com.test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class LoginTek {
	@Test
	public void test() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver",
				"C:\\Users\\User\\Desktop\\Selenium\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();

		driver.manage().window().maximize();
		// ENTER URL
		driver.navigate().to("http://employeemgmt.employee.teknotrait.com/");
		Thread.sleep(3000);
		
		// ENTER USERNAME
		driver.findElement(By.name("username")).sendKeys("dharmendra.patel264@gmail.com");
		Thread.sleep(3000);
		
		// ENTER PASSWORD
		driver.findElement(By.name("pwd")).sendKeys("123456");
		Thread.sleep(3000);
		
		// CLICK ON SIGNIN
		driver.findElement(By.xpath("//button[@class='btn btn-primary hidden-xs signin']")).click();
		Thread.sleep(10000);
		
		// CLICK ON COMPANY POLICY
		driver.findElement(By.xpath("//a[@class='CompanyPolicy']")).click();
		Thread.sleep(5000);
		
		// CLICK ON FILTER BY STATUS
		WebElement wbel_status_filter = driver.findElement(By.xpath("//select[@class='policy_status filter_']"));
		Select sel = new Select(wbel_status_filter);
		sel.selectByVisibleText("Active");
		Thread.sleep(5000);
		
        // Click on Filter button
		
		driver.findElement(By.xpath("//button[@id='filter']")).click();
		Thread.sleep(5000);
		
		//Check the Status 
		
		WebElement wbel_status = driver.findElement(By.xpath("//tr//td[4]"));

		if (wbel_status.getText().trim().equalsIgnoreCase("active")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
        
		//Click on Clear Button
		
		driver.findElement(By.xpath("//button[@id='clearFilter']")).click();
		Thread.sleep(10000);
		/*
		 * WebElement wbel_entries_show =a
		 * driver.findElement(By.xpath("//select[@name='example_length']")); Select sel2
		 * = new Select(wbel_entries_show); sel2.selectByVisibleText("100");
		 * Thread.sleep(4000);
		 */
		
        // Click on Show Entries drop down
		
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@name='example_length']")));
		dropdown.selectByVisibleText("25");
		Thread.sleep(5000);
		
		// To Check the Search option
		
		WebElement search =	driver.findElement(By.xpath("//input[@type='search']"));
		search.sendKeys("Test");
		if (wbel_status.getText().trim().equalsIgnoreCase("Test")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
		
		// Click on Employee Hierarchy and latest news
		Thread.sleep(6000);
		driver.findElement(By.id("view_news")).click();
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("//a[contains(text(),'Latest News')]")).click();
		driver.findElement(By.xpath("//div[@id='tabModal']")).click();
	}
}
